package com.codedecode.casino_project.Services.Implementation;

import com.codedecode.casino_project.Payloads.PlayerDto;
import com.codedecode.casino_project.Payloads.ReportEntryDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.codedecode.casino_project.Entities.Deposit;
import com.codedecode.casino_project.Entities.Player;
import com.codedecode.casino_project.Repositories.DepositRepository;
import com.codedecode.casino_project.Repositories.PlayerRepository;
import com.codedecode.casino_project.Services.PlayerServiceInterface;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PlayerService implements PlayerServiceInterface {

    @Autowired
    private PlayerRepository playerRepository;

    @Autowired
    private DepositRepository depositRepository;

    @Value("${reward.percentage}")
    private double rewardPercentage;

    public double getRewardPercentage() {
        return rewardPercentage / 100.0;
    }

    @Override
    public Player search(String phoneNumber) {

        Player player = playerRepository.findByPhoneNumber(phoneNumber);

        return player;

    }

    @Override
    public void updatePlayerInfo(PlayerDto playerDto, Integer rewardPoints) {

        Player player = playerRepository.findByPhoneNumber(playerDto.getPhoneNumber());
        Deposit deposit = new Deposit();

        player.setName(playerDto.getName());

        deposit.setPlayer(player);
        deposit.setDepositAmount(playerDto.getDepositAmount());
        deposit.setRewardPoints(rewardPoints);

        depositRepository.save(deposit);
        playerRepository.save(player);
    }

    @Override
    public int calculateAndSaveRewardPoints(PlayerDto playerDto) {
        double amountReceived = playerDto.getDepositAmount();
        double rewardPercentage = getRewardPercentage();

        Player player = playerRepository.findByPhoneNumber(playerDto.getPhoneNumber());

//        if (player == null) {
//            // Handling the case where the player does not exist or show an error message
//            throw new PlayerNotFoundException("Player with phone number " + playerDto.getPhoneNumber() + " not found.");
//        }

        Deposit deposit = new Deposit();
        deposit.setPlayer(player);
        deposit.setDepositAmount(amountReceived);
        deposit.setDepositDateTime(LocalDateTime.now());

        int rewardPoints = (int) (amountReceived * rewardPercentage);
        deposit.setRewardPoints(rewardPoints);

        depositRepository.save(deposit);

        return rewardPoints;
    }


    public List<ReportEntryDTO> generateReport(String phoneNumber, String startDate, String endDate) {

        LocalDateTime startDateTime = parseStartDate(startDate);
        LocalDateTime endDateTime = parseEndDate(endDate);

        List<Deposit> searchResults;

        if (phoneNumber != null && !phoneNumber.isEmpty()) {

            Player player = playerRepository.findByPhoneNumber(phoneNumber);

            searchResults = (player != null && startDateTime != null && endDateTime != null)
                    ? depositRepository.findByPlayerPhoneNumberAndDateRange(phoneNumber, startDateTime, endDateTime)
                    : (player != null)
                    ? depositRepository.findByPlayerPhoneNumber(phoneNumber)
                    : Collections.emptyList();

        } else {
            searchResults = depositRepository.findByDateRange(startDateTime, endDateTime);
        }

        return convertToDTOs(searchResults);
    }

    private List<ReportEntryDTO> convertToDTOs(List<Deposit> deposits) {
        return deposits.stream()
                .map(deposit -> convertToDTO(deposit))
                .collect(Collectors.toList());
    }

    private ReportEntryDTO convertToDTO(Deposit deposit) {

        ReportEntryDTO reportDTO = new ReportEntryDTO();
        reportDTO.setPlayerName(deposit.getPlayer().getName());
        reportDTO.setPhoneNumber(deposit.getPlayer().getPhoneNumber());
        reportDTO.setDepositAmount(deposit.getDepositAmount());
        reportDTO.setDateTime(deposit.getDepositDateTime());
        reportDTO.setRewardPoints(deposit.getRewardPoints());

        return reportDTO;
    }

    private LocalDateTime parseStartDate(String startDate) {
        if (startDate != null && !startDate.isEmpty()) {

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
            return LocalDateTime.parse(startDate + "T00:00:00", formatter);
        }
        return null;
    }

    private LocalDateTime parseEndDate(String endDate) {
        if (endDate != null && !endDate.isEmpty()) {

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
            return LocalDateTime.parse(endDate + "T23:59:59", formatter);
        }
        return null;
    }

}
