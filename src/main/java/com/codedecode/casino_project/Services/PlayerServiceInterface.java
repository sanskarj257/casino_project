package com.codedecode.casino_project.Services;

import com.codedecode.casino_project.Entities.Player;
import com.codedecode.casino_project.Payloads.PlayerDto;
import com.codedecode.casino_project.Payloads.ReportEntryDTO;
import java.util.List;

public interface PlayerServiceInterface {

    public Player search(String phoneNumber);

    public void updatePlayerInfo(PlayerDto playerDto, Integer rewardPoints);

    public int calculateAndSaveRewardPoints(PlayerDto playerDto);

    List<ReportEntryDTO> generateReport(String phoneNumber, String startDate, String endDate);
}
