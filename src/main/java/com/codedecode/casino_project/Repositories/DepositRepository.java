package com.codedecode.casino_project.Repositories;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.codedecode.casino_project.Entities.Deposit;

public interface DepositRepository extends JpaRepository<Deposit, Long> {

    @Query("SELECT d FROM Deposit d WHERE d.player.phoneNumber = :phoneNumber")
    public List<Deposit> findByPlayerPhoneNumber(@Param("phoneNumber") String phoneNumber);

    @Query("SELECT d FROM Deposit d WHERE d.depositDateTime BETWEEN :startDate AND :endDate AND d.player.phoneNumber = :phoneNumber")
    public List<Deposit> findByPlayerPhoneNumberAndDateRange(@Param("phoneNumber") String phoneNumber,
                                                             @Param("startDate") LocalDateTime startDate,
                                                             @Param("endDate") LocalDateTime endDate);

    @Query("SELECT d FROM Deposit d WHERE d.depositDateTime BETWEEN :startDate AND :endDate")
    public List<Deposit> findByDateRange(@Param("startDate") LocalDateTime startDate,
                                         @Param("endDate") LocalDateTime endDate);




}
