package com.codedecode.casino_project.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.codedecode.casino_project.Entities.Player;

public interface PlayerRepository extends JpaRepository<Player, String> {

    public Player findByPhoneNumber(String phoneNumber);

}