package com.codedecode.casino_project.Entities;

import java.time.LocalDateTime;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "deposits")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Deposit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "deposit_id")
    private Long depositId;

    
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "player_phone_number", referencedColumnName = "phone_number")
    private Player player;

    @Column(name = "deposit_amount")
    @Min(value = 0, message = "Amount cannot be negative")
    @Max(value = 1000000, message = "Amount cannot be greater than 10,00,000")
    private double depositAmount;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "deposit_date_time")
    private LocalDateTime depositDateTime;

    @Column(name = "reward_points")
    private Integer rewardPoints;

}
