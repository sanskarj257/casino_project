package com.codedecode.casino_project.Exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class GlobalExceptionHandler {

    private final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    ModelAndView modelAndView = new ModelAndView("index");

    @ExceptionHandler(NullPointerException.class)
    public ModelAndView handleNullPointerException(NullPointerException e) {

        logger.error("A null pointer exception occurred: " + e.getMessage(), e);

        modelAndView.addObject("errorMessage", "A null pointer exception occurred: " + e.getMessage());

        return modelAndView;
    }

    @ExceptionHandler(BusinessExceptions.class)
    public ModelAndView handleBusinessException(BusinessExceptions e) {

        logger.error("A business exception occurred: " + e.getMessage(), e);

        modelAndView.addObject("errorMessage", "A business exception occurred: " + e.getMessage());

        return modelAndView;
    }

    @ExceptionHandler(DaoExceptions.class)
    public ModelAndView handleDaoException(DaoExceptions e) {

        logger.error("A DAO exception occurred: " + e.getMessage(), e);

        modelAndView.addObject("errorMessage", "A DAO exception occurred: " + e.getMessage());

        return modelAndView;
    }

    @ExceptionHandler(ControllerExceptions.class)
    public ModelAndView handleControllerException(ControllerExceptions e) {

        logger.error("A controller exception occurred: " + e.getMessage(), e);

        modelAndView.addObject("errorMessage", "A controller exception occurred: " + e.getMessage());

        return modelAndView;
    }

    @ExceptionHandler(Exception.class)
    public ModelAndView handleGlobalException(Exception e) {

        logger.error("An error occurred: " + e.getMessage(), e);

        modelAndView.addObject("errorMessage", "An error occurred: " + e.getMessage());

        return modelAndView;
    }
}
