package com.codedecode.casino_project.Controllers;

import com.codedecode.casino_project.Entities.Player;
import com.codedecode.casino_project.Payloads.PlayerDto;
import com.codedecode.casino_project.Payloads.ReportEntryDTO;
import com.codedecode.casino_project.Services.PlayerServiceInterface;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class PlayerController {

	@Value("${reward.percentage}")
	private double rewardPercentage;

	@Autowired
	private PlayerServiceInterface service;

	ModelAndView modelAndView = new ModelAndView("index");

	@GetMapping("/")
	public ModelAndView showCasinoApp() {

		ModelAndView modelAndView = new ModelAndView("index");

		return modelAndView;
	}

	@PostMapping("/")
	public ModelAndView handleCasinoApp(@RequestParam("action") String action,
			@Valid @RequestParam("phoneNumber") String phoneNumber,
			@Valid @RequestParam(name = "name", required = false) String name,
			@Valid @RequestParam(name = "depositAmount", required = false) Double depositAmount) {

		if ("check".equals(action)) {

			modelAndView.addObject("showFields", true);

			Player player = service.search(phoneNumber);

			if (player != null) {
				modelAndView.addObject("playerPhoneNumber", phoneNumber);
				modelAndView.addObject("foundPlayerName", player.getName());
			} else {
				return modelAndView;
			}

		}
		if ("calculate".equals(action)) {

			PlayerDto playerDto = new PlayerDto(phoneNumber, name, depositAmount);

			int rewardPoints = service.calculateAndSaveRewardPoints(playerDto);

			modelAndView.addObject("rewardPoints", rewardPoints);

		}

		return modelAndView;
	}

	@GetMapping("/report")
	public ModelAndView showReport(@RequestParam(required = false) String playerPhoneNumber,
			@RequestParam(required = false) String startDate,
			@RequestParam(required = false) String endDate) {

		if (playerPhoneNumber == null && startDate == null && endDate == null) {
			// message to be shown
		}

		if (playerPhoneNumber != null || (startDate != null && endDate != null)) {

			List<ReportEntryDTO> searchResults = service.generateReport(playerPhoneNumber, startDate, endDate);

			modelAndView.addObject("searchResults", searchResults);
		}

		modelAndView.addObject("rewardPercentage", rewardPercentage);
		return modelAndView;
	}

	@PutMapping("/admin/update")
	@PreAuthorize("hasRole('ADMIN')")
	public ModelAndView updatePlayer(@Valid @RequestParam("phoneNumber") String phoneNumber,
			@Valid @RequestParam(name = "name", required = false) String name,
			@Valid @RequestParam(name = "depositAmount", required = false) Double depositAmount,
			Integer rewardPoints) {

		PlayerDto playerDto = new PlayerDto(phoneNumber, name, depositAmount);

		service.updatePlayerInfo(playerDto, rewardPoints);

		return modelAndView;
	}

}
