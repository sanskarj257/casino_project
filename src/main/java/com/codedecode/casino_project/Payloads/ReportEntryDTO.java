package com.codedecode.casino_project.Payloads;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReportEntryDTO {

    private String playerName;
    private String phoneNumber;
    private double depositAmount;
    private LocalDateTime dateTime;
    private int rewardPoints;
    private int totalRewardPoints;

}
