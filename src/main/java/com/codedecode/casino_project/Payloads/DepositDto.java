package com.codedecode.casino_project.Payloads;

import com.codedecode.casino_project.Entities.Player;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DepositDto {

    private Player player;

    private LocalDateTime dateTime;

    @Min(value = 0, message = "Amount cannot be negative")
    @Max(value = 1000000, message = "Amount cannot be greater than 1,000,000")
    private double depositAmount;

    private int rewardPoints;
}
