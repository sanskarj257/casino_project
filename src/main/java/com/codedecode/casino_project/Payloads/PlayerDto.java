package com.codedecode.casino_project.Payloads;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PlayerDto {

        @Pattern(regexp = "^[0-9]{10}$", message = "Phone number is not valid")
        private String phoneNumber;

        @NotEmpty
        private String name;

        @Min(value = 0, message = "Amount cannot be negative")
        @Max(value = 1000000, message = "Amount cannot be greater than 10,00,000")
        private double depositAmount;

}
