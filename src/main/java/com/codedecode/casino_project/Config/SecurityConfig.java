package com.codedecode.casino_project.Config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractAuthenticationFilterConfigurer;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class SecurityConfig {

        @Value("${cashier.username}")
        private String cashierUsername;

        @Value("${cashier.password}")
        private String cashierPassword;

        @Value("${admin.username}")
        private String adminUsername;

        @Value("${admin.password}")
        private String adminPassword;

        @Bean
        public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
                http
                                .authorizeHttpRequests(authorize -> authorize
                                                .requestMatchers("/", "/api").hasRole("USER")
                                                .requestMatchers("/admin/**").hasRole("ADMIN")
                                                .anyRequest().authenticated())
                                .formLogin(AbstractAuthenticationFilterConfigurer::permitAll)
                                .rememberMe(Customizer.withDefaults());

                return http.build();
        }

        @Bean
        public UserDetailsService users() {

                String hashedcashierPassword = PasswordEncoderFactories.createDelegatingPasswordEncoder()
                                .encode(cashierPassword);
                String hashedadminPassword = PasswordEncoderFactories.createDelegatingPasswordEncoder()
                                .encode(adminPassword);

                UserDetails user = User.builder()
                                .username(cashierUsername)
                                .password(hashedcashierPassword)
                                .roles("USER")
                                .build();
                UserDetails admin = User.builder()
                                .username(adminUsername)
                                .password(hashedadminPassword)
                                .roles("USER", "ADMIN")
                                .build();
                return new InMemoryUserDetailsManager(user, admin);
        }
}
